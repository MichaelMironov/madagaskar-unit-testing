package com.example;

import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {

    Animal herbivore = new Animal();

    @Test
    void animalGetFoodHerbivoreTest() throws Exception {
        assertEquals(List.of("Трава", "Различные растения"),herbivore.getFood("Травоядное"));
    }

    @Test
    void animalGetWrongFoodTest() throws Exception {
        Throwable throwError = assertThrows(Exception.class, ()->{
            Animal animal = new Animal();
            animal.getFood("краб");
                }
        );

        assertEquals("Неизвестный вид животного, используйте значение Травоядное или Хищник",throwError.getMessage());
    }

    @Test
    void animalGetFamilyTest() {
        assertEquals("Существует несколько семейств: заячьи, беличьи, мышиные, кошачьи, псовые, медвежьи, куньи", herbivore.getFamily());
    }
}