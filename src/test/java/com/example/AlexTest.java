package com.example;

import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AlexTest {
    Alex alex= new Alex();
    @Test
    void alexGetKittensTest() {
        assertEquals(0, alex.getKittens());
    }

    @Test
    void alexGetFriendsTest() {
        assertEquals(List.of("Марти", "Глория", "Мелман"), alex.getFriends());
    }

    @Test
    void alexGetPlaceOfLivingTest() {
        assertEquals("Нью-Йоркский зоопарк", alex.getPlaceOfLiving());
    }

    @Test
    void alexCtorTest() throws Exception {
        Feline feline = new Feline();
        Alex alex = new Alex(feline,"Самец");

        assertTrue(alex instanceof Lion);
    }
}