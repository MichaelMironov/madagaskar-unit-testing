package com.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class LionTest {

    Feline feline = new Feline();
    Lion male = new Lion(feline,"Самец");
    Lion female = new Lion(feline,"Самка");

    @Mock
    Lion mockLion = new Lion(feline,"Самец");

    LionTest() throws Exception {
    }

    @Test
    void lionGetKittensTest() {
        assertEquals(feline.getKittens(), male.getKittens());
    }

    @Test
    void lionHaveManeTest() {
        assertTrue(male.hasMane);
        assertFalse(female.hasMane);
        assertTrue(male.doesHaveMane());
        assertFalse(female.doesHaveMane());
        Throwable thrown = assertThrows(Exception.class, ()->{
            Lion lion = new Lion(feline, "nope");
        });

        assertEquals("Используйте допустимые значения пола животного - самец или самка",thrown.getMessage());
    }

    @Test
    void lionGetFoodTest() throws Exception {
        Mockito.when(mockLion.getFood()).thenReturn(List.of("Животные", "Птицы", "Рыба"));
        assertEquals(mockLion.getFood(), female.getFood());
    }


}