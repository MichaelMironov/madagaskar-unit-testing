package com.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FelineTest {

    Feline feline = new Feline();
    Animal animal = new Animal();

    @Test
    void felineEatMeatTest() throws Exception {
        assertEquals(animal.getFood("Хищник") ,feline.eatMeat());
    }

    @Test
    void felineGetFamilyTest() {
        assertEquals("Кошачьи", feline.getFamily());
    }

    @Test
    void felineGetKittensTest() {
        assertEquals(1, feline.getKittens());
    }

    @Test
    void felineGetKittensParametrisedTest() {
        assertEquals(5, feline.getKittens(5));
    }
}