package com.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CatTest {

    Feline feline = new Feline();

    @Test
    void catGetSoundTest() {
        Cat catSound = new Cat(feline);
        assertEquals("Мяу", catSound.getSound());
    }

    @Test
    void catGetFoodTest() throws Exception {
        Cat fel = new Cat(feline);
        assertEquals(List.of("Животные", "Птицы", "Рыба"), fel.getFood());
    }

    @Test
    void catCtorTest(){
        Cat cat = new Cat(feline);
        assertNotNull(cat.predator);
    }

 }